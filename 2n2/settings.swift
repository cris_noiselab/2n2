//
//  settings.swift
//  2n2
//
//  Created by Cristopher Nunez Del Prado on 24/02/17.
//  Copyright © 2017 Cristopher Nunez Del Prado. All rights reserved.
//

import Foundation
import UIKit
import GameKit
import Firebase

class settings: UIViewController {
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Reachability.isConnectedToNetwork() == true
        {

            bannerView.adUnitID="ca-app-pub-1462291695436824/3215895012"
            bannerView.rootViewController=self
            bannerView.load(GADRequest())
        }
    }
    
    
    
    @IBAction func showTutorial1(_ sender: Any) {
        showTutorialNervous(action: 1)
    }
    
    func showTutorialNervous(action:Int){
        if action==1{
            let  alert = UIAlertController(title: "Tutorial Nervioso", message: "Los jugadores deben ponerse en cada lado del dispositivo.                Cuando se inicia el juego, la estrella indica el turno de lanzamiento de cada jugador.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Siguiente", style: UIAlertActionStyle.default, handler: { action in self.showTutorialNervous(action: 2)}))
            
            
            self.present(alert, animated: true, completion: nil)
            
        }
        if action==2{
            let  alert = UIAlertController(title: "Tutorial Nervioso", message: "El botón con la flecha azul debe ser presionado para lanzar una carta y el botón con la mano roja debe ser presionado cuando la carta en la mesa coincide con la carta del contador. Si la carta en la mesa es igual a la del contador, de caso contrario estas van para el jugador que realizo el golpe. La flecha roja indica el jugador que recibe las cartas.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Siguiente", style: UIAlertActionStyle.default, handler: { action in self.showTutorialNervous(action: 3)}))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        if action==2{
            let  alert = UIAlertController(title: "Tutorial Nervioso", message: "Cuando las cartas se acabaron el jugador debe seguir jugando hasta acumular 3 checks. Se gana un check cuando no se tiene cartas y se logra hacer un punto. Si el jugador con checks es castigado pierde todos sus checks.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    @IBAction func showTutorial2(_ sender: Any) {
    }
    
    

}
