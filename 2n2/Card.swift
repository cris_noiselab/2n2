//
//  Card.swift
//  2n2
//
//  Created by Cristopher Nunez Del Prado on 31/01/17.
//  Copyright © 2017 Cristopher Nunez Del Prado. All rights reserved.
//

import Foundation
class Card{
    var number:Int
    var icon:String
    var imageName:String
    init(r_number:Int, r_icon:String, r_imageName:String){
        number=r_number
        icon=r_icon
        imageName=r_imageName
        
    }
    
    
}
