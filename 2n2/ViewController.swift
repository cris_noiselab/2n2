//
//  ViewController.swift
//  2n2
//
//  Created by Cristopher Nunez Del Prado on 30/01/17.
//  Copyright © 2017 Cristopher Nunez Del Prado. All rights reserved.
//

import UIKit
import GameKit
import Firebase

class ViewController: UIViewController {
    
    
    //++++++
    //++++++
    //++++++
    var interstitial: GADInterstitial!
    
    @IBOutlet weak var counterReverse: UILabel!
    @IBOutlet weak var checkBoxReverse: UIImageView!
    
    @IBOutlet weak var starIndicatorReverse: UIImageView!
    
    @IBOutlet weak var indicatorReverse: UILabel!
    @IBOutlet weak var cardSpace: UIImageView!
    @IBOutlet weak var settingsContainer: UIView!
    @IBOutlet weak var counter: UILabel!
    @IBOutlet weak var checkBox: UIImageView!
    
    @IBOutlet weak var starIndicator: UIImageView!
    @IBOutlet weak var indicator: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var launhReveseButton: UIButton!
    
    @IBOutlet weak var hitButton: UIButton!
    @IBOutlet weak var launchButton: UIButton!
    @IBOutlet weak var hitReverseButton: UIButton!
    //++++++
    @IBOutlet weak var pun: UIImageView!
    //++++++
    
    @IBOutlet weak var punReverse: UIImageView!
    //++++++
    
    
    @IBAction func hit(_ sender: Any) {
        
        if centralBuch.count>0{
            if playerWhoHit == 0{
                playerWhoHit=1
            }
            if playerWhoHit==1{
                var countCards=centralBuch
                var lastItem=(centralBuch.count)-1
                var currentBunchNumber = centralBuch[lastItem].number
                
                if counterVar==currentBunchNumber{
                    shuffleCentralBunch()
                    
                    
                    if player2.checks>0{
                        player2.checks=0
                        
                    }
                    punnish(player: 2)
                    centralBuch.removeAll()
                    self.cardSpace.image=UIImage(named: "blankcard.png")
                    
                    if player1.cards.count==0{
                        player1.checks=player1.checks+1
                        checkCheks(player: player1)
                    }
                    turn=2
                    showPun(pun: turn)
                    
                    
                    
                }
                else{
                    shuffleCentralBunch()
                    
                    punnish(player: 1)
                    centralBuch.removeAll()
                    self.cardSpace.image=UIImage(named: "blankcard.png")
                    if player1.checks>0{
                        player1.checks=0
                        
                    }
                    turn=1
                     showPun(pun: turn)
                    
                    
                    
                }
                updateChecks()
                changeTurnIndicator(turn: turn)
                counterVar=0
                self.counter.text = String(counterVar)
                self.counterReverse.text = String(counterVar)
                playerWhoHit=0
                updateIndicators()
                
                
            }
            lockOtherLaunchFunc()

        }
        
        

    }
    @IBAction func hitReverse(_ sender: Any) {
        
        if centralBuch.count>0{
            if playerWhoHit == 0{
                playerWhoHit=1
            }
            if playerWhoHit==1{
                var countCards=centralBuch
                var lastItem=(centralBuch.count)-1
                var currentBunchNumber = centralBuch[lastItem].number
                
                if counterVar==currentBunchNumber{
                    shuffleCentralBunch()
                    
                    
                    if player1.checks>0{
                        player1.checks=0
                        
                    }
                    punnish(player: 1)
                    centralBuch.removeAll()
                    self.cardSpace.image=UIImage(named: "blankcard.png")
                    
                    if player2.cards.count==0{
                        player2.checks=player2.checks+1
                        checkCheks(player: player2)
                    }
                    turn=1
                     showPun(pun: turn)
                    
                    
                    
                }
                else{
                    shuffleCentralBunch()
                    
                    punnish(player: 2)
                    centralBuch.removeAll()
                    self.cardSpace.image=UIImage(named: "blankcard.png")
                    if player2.checks>0{
                        player2.checks=0
                        
                    }
                    turn=2
                     showPun(pun: turn)
                    
                    
                    
                }
                updateChecks()
                counterVar=0
                changeTurnIndicator(turn: turn)
                self.counter.text = String(counterVar)
                self.counterReverse.text = String(counterVar)
                playerWhoHit=0
                updateIndicators()
                
            }
            lockOtherLaunchFunc()

            
        }
        
        
    }
    
    
    @IBAction func launchReverse(_ sender: Any) {
        
        if player1.cards.count == 0 && player2.cards.count == 0{
            verifyNoCardsSituation()
            
        }
        else{
            var cardToLauch:Card
            
            cardToLauch=player2.getFirstCard()
            loadFromUser(card: cardToLauch)
            player2.deleteFirstCard()
            counterVar=counterVar+1
            updateCentralCard()
            if player1.hasCards(){
                changeTurn()
            }
            updateCounter()
        }
        
        

    }
    @IBAction func startButton(_ sender: Any) {
        
        
        
        createAllCards()
        print("cards created")
        shuffleCentralBunch()
        print("bunch shuffled")
        splitCards()
        print("cards splited")
        
        starIndicator.isHidden=false
        starIndicatorReverse.isHidden=false
        defineFirstTurn()
        
        self.startButton.isHidden = true
        unlockAllButtons()
        lockOtherLaunchFunc()
        updateChecks()
    }
    
    @IBAction func stopButton(_ sender: Any) {
        
        let  alert = UIAlertController(title: "Configuraciones", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Continuar", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Reiniciar Juego", style: UIAlertActionStyle.default, handler: { action in self.confirmAction(action: 1)}))
        alert.addAction(UIAlertAction(title: "Terminar Juego", style: UIAlertActionStyle.default, handler: { action in self.confirmAction(action: 2)}))
        
        self.present(alert, animated: true, completion: nil)
    }
    func confirmAction(action:Int){
        if action==1{
            let  alert = UIAlertController(title: "Alerta!", message: "Realmente desea Reiniciar el Juego?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Si", style: UIAlertActionStyle.default, handler: { action in self.restartGame()}))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        if action==2{
            let  alert = UIAlertController(title: "Alerta!", message: "Realmente desea Salir del Juego?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Si", style: UIAlertActionStyle.default, handler: { action in self.performSegue(withIdentifier: "goMainMenu", sender: self)}))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
        
    }
    
    @IBAction func launch(_ sender: Any) {
        
        if player1.cards.count == 0 && player2.cards.count == 0{
            verifyNoCardsSituation()
            
        }
        else{
            var cardToLauch:Card
            cardToLauch=player1.getFirstCard()
            loadFromUser(card: cardToLauch)
            player1.deleteFirstCard()
            counterVar=counterVar+1
            updateCentralCard()
            if player2.hasCards(){
                changeTurn()
            }
            updateCounter()

            
        }
        

    }
   
    
    var centralBuch =  [Card]()
    var player1 = Player(r_id: 1, r_checks: 0)
    var player2 = Player(r_id: 2, r_checks: 0)
    var turn=0
    var playerWhoHit=0
    var counterVar=0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //----Ads
        if Reachability.isConnectedToNetwork() == true
        {
            self.interstitial = self.createAndLoadAd()
        }
        
        
        //----
        
        
        counterReverse.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
        indicatorReverse.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
        lockAllButtons()
        starIndicator.isHidden=true
        starIndicatorReverse.isHidden=true
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

        
    }
    
    
    //-----Ads
    func createAndLoadAd() -> GADInterstitial
    {
        var ad = GADInterstitial(adUnitID: "ca-app-pub-1462291695436824/5260492477")
        
        
        var request = GADRequest()
        
        request.testDevices = ["2077ef9a63d2b398840261c8221a0c9b"]
        
        ad.load(request)
        return ad
 
        /*var interstitial = GADInterstitial(adUnitID: "ca-app-pub-4708498702836503/8147076471")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial*/
        
    }
    func interstitialDidDismissScreen(ad: GADInterstitial!) {
       
        if Reachability.isConnectedToNetwork() == true
        {
             interstitial = createAndLoadAd()
        }
    }
    
    
    func presentAd(){
        var x = 0
        while x==0{
            if (self.interstitial.isReady)
            {
                self.interstitial.present(fromRootViewController: self)
                if Reachability.isConnectedToNetwork() == true
                {
                    interstitial = createAndLoadAd()
                }
                self.interstitial = self.createAndLoadAd()
                x=1
            }
        }
        
    }
    
    //--------
    
    
    func changeTurn(){
        if turn==1{
            turn=2
        }
        else{
            turn=1
        }
        changeTurnIndicator(turn: turn)
        lockOtherLaunchFunc()
    }
    func updateTurnIcon(){
        
    }
    func defineFirstTurn(){
        var randomNumber = Int(arc4random_uniform(40) + 10)
        if randomNumber % 2 == 0{
            randomNumber=2
        }
        else{
            randomNumber=1
        }
        turn=randomNumber
        changeTurnIndicator(turn: turn)
        lockOtherLaunchFunc()
    }
    func changeTurnIndicator(turn:Int){
        if turn==1{
            starIndicator.isHidden=false
            starIndicatorReverse.isHidden=true
        }
        else{
            starIndicator.isHidden=true
            starIndicatorReverse.isHidden=false
            
        }
    }
    func checkCheks(player:Player){
        if player.checks==3{
            congratulateWinner(player: player)
        }
    
    }
    func congratulateWinner(player:Player){
        let  alert = UIAlertController(title: "Felicidades!", message: "El jugador \(player.id) es el ganador", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Reiniciar Juego", style: UIAlertActionStyle.default, handler: { action in self.restartGame()}))
        alert.addAction(UIAlertAction(title: "Salir", style: UIAlertActionStyle.default, handler: { action in self.performSegue(withIdentifier: "goMainMenu", sender: self)}))
        self.present(alert, animated: true, completion: nil)
    }
    func deleteEverything(){
        
    }
    
    func shuffleCentralBunch(){
        centralBuch = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: centralBuch) as! [Card]
        //centralBuch = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: centralBuch) as! [Card]
        
        
    }
    func splitCards(){
        var temporal=centralBuch[0..<26]
        player1.cards = Array(temporal)
        temporal=centralBuch[26..<52]
        player2.cards = Array(temporal)
        removeAllFromCentralBunch()

    }
    func punnish(player:Int){
        if player==1{
            player1.receivePunishment(bunch: centralBuch)
            
        }
        if player==2{
            player2.receivePunishment(bunch: centralBuch)
        }
        removeAllFromCentralBunch()
        
        
    }
    func removeAllFromCentralBunch(){
        centralBuch.removeAll()
    }
    
    func loadFromUser(card:Card){
        centralBuch.append(card)
        
    }
    func setHitter(hitter:Int){
        playerWhoHit=hitter
    }
    
    
    
    func hit(player:Int){
        setHitter(hitter: 1)
        var countCards=centralBuch
        var lastItem=centralBuch.count
        var currentBunchNumber = centralBuch[lastItem].number
        
        if counterVar==currentBunchNumber{
            shuffleCentralBunch()
            
            
                if player2.checks>0{
                    player2.checks=0
                    
                }
                punnish(player: 2)
                centralBuch.removeAll()
                self.cardSpace.image=UIImage(named: "blankcard.png")
            
                if player1.cards.count==0{
                    player1.checks=player1.checks+1
                    checkCheks(player: player1)
                }
                
            
            
        }
        else{
            shuffleCentralBunch()
            
                punnish(player: 1)
                centralBuch.removeAll()
                self.cardSpace.image=UIImage(named: "blankcard.png")
                if player1.checks>0{
                    player1.checks=0
                    
                }
                
        
            
        }
        
        
        
        
        
        
    }
    
    func updateCentralCard(){
        var card=centralBuch[(centralBuch.count)-1]
        self.cardSpace.image=UIImage(named: "\(card.imageName).png")
        
        
    }
    
    
    
    
    
    
    
    func createAllCards(){
        centralBuch.append(Card(r_number: 1, r_icon: "Spades", r_imageName: "S1"))
        centralBuch.append(Card(r_number: 2, r_icon: "Spades", r_imageName: "S2"))
        centralBuch.append(Card(r_number: 3, r_icon: "Spades", r_imageName: "S3"))
        centralBuch.append(Card(r_number: 4, r_icon: "Spades", r_imageName: "S4"))
        centralBuch.append(Card(r_number: 5, r_icon: "Spades", r_imageName: "S5"))
        centralBuch.append(Card(r_number: 6, r_icon: "Spades", r_imageName: "S6"))
        centralBuch.append(Card(r_number: 7, r_icon: "Spades", r_imageName: "S7"))
        centralBuch.append(Card(r_number: 8, r_icon: "Spades", r_imageName: "S8"))
        centralBuch.append(Card(r_number: 9, r_icon: "Spades", r_imageName: "S9"))
        centralBuch.append(Card(r_number: 10, r_icon: "Spades", r_imageName: "S10"))
        centralBuch.append(Card(r_number: 11, r_icon: "Spades", r_imageName: "S11"))
        centralBuch.append(Card(r_number: 12, r_icon: "Spades", r_imageName: "S12"))
        centralBuch.append(Card(r_number: 13, r_icon: "Spades", r_imageName: "S13"))
        
        
        
        centralBuch.append(Card(r_number: 1, r_icon: "Trefoil", r_imageName: "T1"))
        centralBuch.append(Card(r_number: 2, r_icon: "Trefoil", r_imageName: "T2"))
        centralBuch.append(Card(r_number: 3, r_icon: "Trefoil", r_imageName: "T3"))
        centralBuch.append(Card(r_number: 4, r_icon: "Trefoil", r_imageName: "T4"))
        centralBuch.append(Card(r_number: 5, r_icon: "Trefoil", r_imageName: "T5"))
        centralBuch.append(Card(r_number: 6, r_icon: "Trefoil", r_imageName: "T6"))
        centralBuch.append(Card(r_number: 7, r_icon: "Trefoil", r_imageName: "T7"))
        centralBuch.append(Card(r_number: 8, r_icon: "Trefoil", r_imageName: "T8"))
        centralBuch.append(Card(r_number: 9, r_icon: "Trefoil", r_imageName: "T9"))
        centralBuch.append(Card(r_number: 10, r_icon: "Trefoil", r_imageName: "T10"))
        centralBuch.append(Card(r_number: 11, r_icon: "Trefoil", r_imageName: "T11"))
        centralBuch.append(Card(r_number: 12, r_icon: "Trefoil", r_imageName: "T12"))
        centralBuch.append(Card(r_number: 13, r_icon: "Trefoil", r_imageName: "T13"))
        
        
        centralBuch.append(Card(r_number: 1, r_icon: "Heart", r_imageName: "H1"))
        centralBuch.append(Card(r_number: 2, r_icon: "Heart", r_imageName: "H2"))
        centralBuch.append(Card(r_number: 3, r_icon: "Heart", r_imageName: "H3"))
        centralBuch.append(Card(r_number: 4, r_icon: "Heart", r_imageName: "H4"))
        centralBuch.append(Card(r_number: 5, r_icon: "Heart", r_imageName: "H5"))
        centralBuch.append(Card(r_number: 6, r_icon: "Heart", r_imageName: "H6"))
        centralBuch.append(Card(r_number: 7, r_icon: "Heart", r_imageName: "H7"))
        centralBuch.append(Card(r_number: 8, r_icon: "Heart", r_imageName: "H8"))
        centralBuch.append(Card(r_number: 9, r_icon: "Heart", r_imageName: "H9"))
        centralBuch.append(Card(r_number: 10, r_icon: "Heart", r_imageName: "H10"))
        centralBuch.append(Card(r_number: 11, r_icon: "Heart", r_imageName: "H11"))
        centralBuch.append(Card(r_number: 12, r_icon: "Heart", r_imageName: "H12"))
        centralBuch.append(Card(r_number: 13, r_icon: "Heart", r_imageName: "H13"))
        
        
        centralBuch.append(Card(r_number: 1, r_icon: "Diamond", r_imageName: "D1"))
        centralBuch.append(Card(r_number: 2, r_icon: "Diamond", r_imageName: "D2"))
        centralBuch.append(Card(r_number: 3, r_icon: "Diamond", r_imageName: "D3"))
        centralBuch.append(Card(r_number: 4, r_icon: "Diamond", r_imageName: "D4"))
        centralBuch.append(Card(r_number: 5, r_icon: "Diamond", r_imageName: "D5"))
        centralBuch.append(Card(r_number: 6, r_icon: "Diamond", r_imageName: "D6"))
        centralBuch.append(Card(r_number: 7, r_icon: "Diamond", r_imageName: "D7"))
        centralBuch.append(Card(r_number: 8, r_icon: "Diamond", r_imageName: "D8"))
        centralBuch.append(Card(r_number: 9, r_icon: "Diamond", r_imageName: "D9"))
        centralBuch.append(Card(r_number: 10, r_icon: "Diamond", r_imageName: "D10"))
        centralBuch.append(Card(r_number: 11, r_icon: "Diamond", r_imageName: "D11"))
        centralBuch.append(Card(r_number: 12, r_icon: "Diamond", r_imageName: "D12"))
        centralBuch.append(Card(r_number: 13, r_icon: "Diamond", r_imageName: "D13"))
        
    }
    func lockOtherLaunchFunc(){
        if turn == 1 {
            self.launhReveseButton.isEnabled = false
             self.launchButton.isEnabled = true
        }
        else{
            self.launhReveseButton.isEnabled = true
             self.launchButton.isEnabled = false
        }
        
    }
    func lockAllButtons(){
        self.hitButton.isEnabled = false
        self.hitReverseButton.isEnabled = false
        self.launchButton.isEnabled = false
        self.launhReveseButton.isEnabled = false
        
    }
    func unlockAllButtons(){
        self.hitButton.isEnabled = true
        self.hitReverseButton.isEnabled = true
        self.launchButton.isEnabled = true
        self.launhReveseButton.isEnabled = true
    }
    func updateCounter(){
        var stringNumber=self.counter.text!
        
        stringNumber=stringNumber.replacingOccurrences(of: "Optional(", with: "")
        stringNumber=stringNumber.replacingOccurrences(of: ")", with: "")
        

        var counterNumber = Int(stringNumber)
        counterNumber=counterNumber!+1
        if counterNumber==14{
            counterNumber=1
        }
        counterVar=counterNumber!
        
        
        stringNumber=String(describing: counterNumber)
        stringNumber=stringNumber.replacingOccurrences(of: "Optional(", with: "")
        stringNumber=stringNumber.replacingOccurrences(of: ")", with: "")
        self.counter.text=stringNumber
        self.counterReverse.text=stringNumber
        
    }
    func restartGame(){
        removeAllFromCentralBunch()
        player1.cards.removeAll()
        player2.cards.removeAll()
        self.cardSpace.image=UIImage(named: "blankcard.png")
        
        player1.checks=0
        player2.checks=0
        turn=0
        playerWhoHit=0
        counterVar=0
        lockAllButtons()
        self.startButton.isHidden = false
        self.starIndicator.isHidden=true
        self.starIndicatorReverse.isHidden = true
        self.counter.text = String(counterVar)
        self.counterReverse.text = String(counterVar)
        updateChecks()
        if Reachability.isConnectedToNetwork() == true{
            if self.interstitial.isReady{
                
                presentAd()
            }
        }

        
    }
    
    func noCards(){
        if player1.cards.count==0 && player1.cards.count==0{
            
            shuffleCentralBunch()
            
            splitCards()
            
        }
        
    }
    func updateChecks(){
        if player1.checks==0{
            self.checkBox.image=UIImage(named: "box.png")
        }
        if player1.checks==1{
            self.checkBox.image=UIImage(named: "oneCheck.png")
        }
        if player1.checks==2{
            self.checkBox.image=UIImage(named: "twoChecks.png")
        }
        if player1.checks==3{
            self.checkBox.image=UIImage(named: "threeChecks.png")
        }
        
        
        if player2.checks==0{
            self.checkBoxReverse.image=UIImage(named: "boxReverse.png")
        }
        if player2.checks==1{
            self.checkBoxReverse.image=UIImage(named: "oneCheckReverse.png")
        }
        if player2.checks==2{
            self.checkBoxReverse.image=UIImage(named: "twoChecksReverse.png")
        }
        if player2.checks==3{
            self.checkBoxReverse.image=UIImage(named: "threeChecksReverse.png")
        }
    }
    
    
    func updateIndicators(){
        var checksLeft = 3-(player1.checks)
        self.indicator.text="\(checksLeft) check(s) para ganar"
        checksLeft = 3-(player2.checks)
        self.indicatorReverse.text="\(checksLeft) check(s) para ganar"
    }
    
    func verifyNoCardsSituation(){
        if player1.checks<player2.checks{
            congratulateWinner(player: player2)
        }
        if player1.checks>player2.checks{
            congratulateWinner(player: player1)
        }
        
        if player1.checks == player2.checks{
            showTiedGame()
        }
        
    }
    
    func showTiedGame(){
        let  alert = UIAlertController(title: "Empate!", message: "Se terminaron las cartas y no hay un ganador...", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Reiniciar Juego", style: UIAlertActionStyle.default, handler: { action in self.restartGame()}))
        alert.addAction(UIAlertAction(title: "Salir", style: UIAlertActionStyle.default, handler: { action in self.performSegue(withIdentifier: "goMainMenu", sender: self)}))
        self.present(alert, animated: true, completion: nil)
    
    }
    
    func showPun(pun:Int){
        if pun==1{
            self.pun.isHidden = false
            UIView.animate(withDuration: 0.5, delay:0.5, options:UIViewAnimationOptions.transitionFlipFromTop, animations: {
                self.pun.alpha = 0
            }, completion: { finished in
                self.pun.isHidden = true
                self.pun.alpha = 1
            })
        }
        if pun==2{
            self.punReverse.isHidden = false
            UIView.animate(withDuration: 0.5, delay:0.5, options:UIViewAnimationOptions.transitionFlipFromTop, animations: {
                self.punReverse.alpha = 0
            }, completion: { finished in
                self.punReverse.isHidden = true
                self.punReverse.alpha = 1
            })
            
        }
    }
    
        
    
    
}

