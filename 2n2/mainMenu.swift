//
//  mainMenu.swift
//  2n2
//
//  Created by Cristopher Nunez Del Prado on 3/03/17.
//  Copyright © 2017 Cristopher Nunez Del Prado. All rights reserved.
//

import Foundation
import UIKit
import GameKit
import Firebase

class mainMenu: UIViewController  {
    @IBOutlet weak var bannerView: GADBannerView!
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        if Reachability.isConnectedToNetwork() == true
        {
            bannerView.adUnitID="ca-app-pub-1462291695436824/3215895012"
            bannerView.rootViewController=self
            bannerView.load(GADRequest())
        }

    
    
    }
    @IBAction func openItunes(_ sender: Any) {
        //UIApplication.shared.openURL(NSURL(string: "itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4")! as URL)
        //UIApplication.shared.openURL(NSURL(string: "itms-apps://itunes.apple.com/app/bars/id706081574")! as URL)
        //UIApplication.shared.openURL(NSURL(string: "https://itunes.apple.com/pe/developer/cristopher-nunez-del-prado/id1209583530")! as URL)
        UIApplication.shared.openURL(NSURL(string: "itms-apps://itunes.apple.com/pe/developer/cristopher-nunez-del-prado/id1209583530")! as URL)
    }
    
    @IBAction func openFacebook(_ sender: Any) {
        let facebookURL = NSURL(string: "fb://profile/134145790574321")!
        if UIApplication.shared.canOpenURL(facebookURL as URL) {
            UIApplication.shared.openURL(facebookURL as URL)
        }
        else {
            UIApplication.shared.openURL(NSURL(string: "https://www.facebook.com/Bit-Room-134145790574321")! as URL)
        }
        
    }
    
    
    
    
    
}

