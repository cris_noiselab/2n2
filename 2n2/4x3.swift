//
//  4x3.swift
//  2n2
//
//  Created by Cristopher Nunez Del Prado on 13/02/17.
//  Copyright © 2017 Cristopher Nunez Del Prado. All rights reserved.
//

import Foundation
import UIKit
import GameKit
import Firebase

class fourThree: UIViewController {
    
     var interstitial: GADInterstitial!
    var turn = 0
    var array: [[Int]] = [ [0,0,0,0,0], [0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0] ]
    var winner=0
    var level = 1
    
    @IBOutlet weak var counter1: UILabel!
    @IBOutlet weak var counter2: UILabel!
    @IBOutlet weak var indicator1: UIImageView!
    @IBOutlet weak var indicator2: UIImageView!
    
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    
    
    @IBOutlet weak var c00: UIImageView!
    @IBOutlet weak var c10: UIImageView!
    @IBOutlet weak var c20: UIImageView!
    @IBOutlet weak var c30: UIImageView!
    @IBOutlet weak var c40: UIImageView!
    @IBOutlet weak var c01: UIImageView!
    @IBOutlet weak var c11: UIImageView!
    @IBOutlet weak var c21: UIImageView!
    @IBOutlet weak var c31: UIImageView!
    @IBOutlet weak var c41: UIImageView!
    @IBOutlet weak var c02: UIImageView!
    @IBOutlet weak var c12: UIImageView!
    @IBOutlet weak var c22: UIImageView!
    @IBOutlet weak var c32: UIImageView!
    @IBOutlet weak var c42: UIImageView!
    @IBOutlet weak var c03: UIImageView!
    @IBOutlet weak var c13: UIImageView!
    @IBOutlet weak var c23: UIImageView!
    @IBOutlet weak var c33: UIImageView!
    @IBOutlet weak var c43: UIImageView!
    @IBOutlet weak var c04: UIImageView!
    @IBOutlet weak var c14: UIImageView!
    @IBOutlet weak var c24: UIImageView!
    @IBOutlet weak var c34: UIImageView!
    @IBOutlet weak var c44: UIImageView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //----Ads
        
        
        if Reachability.isConnectedToNetwork() == true
        {
            self.interstitial = self.createAndLoadAd()
        }
        
        //----
        defineFirstTurn()
        updateBackgrounds()
        
    }
    //-----Ads
    func createAndLoadAd() -> GADInterstitial
    {
        var ad = GADInterstitial(adUnitID: "ca-app-pub-1462291695436824/5260492477")
        
        
        var request = GADRequest()
        
        request.testDevices = ["2077ef9a63d2b398840261c8221a0c9b"]
        
        ad.load(request)
        return ad
        
        /*var interstitial = GADInterstitial(adUnitID: "ca-app-pub-4708498702836503/8147076471")
         interstitial.delegate = self
         interstitial.load(GADRequest())
         return interstitial*/
        
    }
    func interstitialDidDismissScreen(ad: GADInterstitial!) {
        
        if Reachability.isConnectedToNetwork() == true
        {
            interstitial = createAndLoadAd()
        }
    }
    
    
    func presentAd(){
        var x = 0
        while x==0{
            if (self.interstitial.isReady)
            {
                self.interstitial.present(fromRootViewController: self)
                
                
                
                if Reachability.isConnectedToNetwork() == true
                {
                    self.interstitial = self.createAndLoadAd()                }
                x=1
            }
        }
        
    }
    
    //--------
    
    @IBAction func stopGame(_ sender: Any) {
        let  alert = UIAlertController(title: "Configuraciones", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Continuar", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Reiniciar Juego", style: UIAlertActionStyle.default, handler: { action in self.confirmAction(action: 1)}))
        alert.addAction(UIAlertAction(title: "Terminar Juego", style: UIAlertActionStyle.default, handler: { action in self.confirmAction(action: 2)}))
        
        self.present(alert, animated: true, completion: nil)
    }
    func confirmAction(action:Int){
        if action==1{
            let  alert = UIAlertController(title: "Alerta!", message: "Realmente desea Reiniciar el Juego?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Si", style: UIAlertActionStyle.default, handler: { action in self.restartGame()}))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        if action==2{
            let  alert = UIAlertController(title: "Alerta!", message: "Realmente desea Salir del Juego?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Si", style: UIAlertActionStyle.default, handler: { action in self.performSegue(withIdentifier: "goMainMenu", sender: self)}))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }

    
    @IBAction func onePressed(_ sender: Any) {
        addValue(row: 0)
    }
    @IBAction func twoPressed(_ sender: Any) {
        addValue(row: 1)
    }
    
    @IBAction func threePressed(_ sender: Any) {
        addValue(row: 2)
    }
    @IBAction func fourPressed(_ sender: Any) {
        addValue(row: 3)
    }
    @IBAction func fivePressed(_ sender: Any) {
        addValue(row: 4)
    }
    
    
    func changeTurn(){
        if turn==1{
            turn=2
        }
        else
        {if turn==2{
            turn=1
            }
        }
        changeTurnIndicator()
    }
    func defineFirstTurn(){
        var randomNumber = Int(arc4random_uniform(40) + 10)
        if randomNumber % 2 == 0{
            randomNumber=2
        }
        else{
            randomNumber=1
        }
        turn=randomNumber
        changeTurnIndicator()
    }
    func changeTurnIndicator(){
        if turn==1{
            indicator1.isHidden=false
            indicator2.isHidden=true
        }
        else{
            indicator1.isHidden=true
            indicator2.isHidden=false
            
        }
    }
    
    func checkWinner(){
        var checkWinner=0
        for x in 0 ..< array.count {
            for y in 0 ..< array[x].count {
                if array[x][y] != 0 && winner == 0
                {
                    if y+3<5{
                        
                        if array[x][y] == array[x][y+1] && array[x][y+1] == array[x][y+2] && array[x][y+2] == array[x][y+3]{
                            checkWinner=array[x][y]
                            array[x][y] = 3
                            array[x][y+1] = 3
                            array[x][y+2] = 3
                            array[x][y+3] = 3
                            congratulatePlayer(winner: checkWinner)
                        }
                        
                    }
                    if y-3 > -1{
                        
                        if array[x][y] == array[x][y-1] && array[x][y-1] == array[x][y-2] && array[x][y-2] == array[x][y-3]{
                            checkWinner=array[x][y]
                            array[x][y] = 3
                            array[x][y-1] = 3
                            array[x][y-2] = 3
                            array[x][y-3] = 3
                            congratulatePlayer(winner: checkWinner)
                        }
                        
                    }
                    if x-3 > -1{
                        if array[x][y] == array[x-1][y] && array[x-1][y] == array[x-2][y] && array[x-2][y] == array[x-3][y]{
                            checkWinner=array[x][y]
                            array[x][y] = 3
                            array[x-1][y] = 3
                            array[x-2][y] = 3
                            array[x-3][y] = 3
                            congratulatePlayer(winner: checkWinner)                        }
                        
                    }
                    if x+3<5{
                        if array[x][y] == array[x+1][y] && array[x+1][y] == array[x+2][y] && array[x+2][y] == array[x+3][y]{
                            checkWinner=array[x][y]
                            array[x][y] = 3
                            array[x+1][y] = 3
                            array[x+2][y] = 3
                            array[x+3][y] = 3
                            congratulatePlayer(winner: checkWinner)
                        }
                        
                    }
                    if y+3<5 && x-3 > -1{
                        if array[x][y] == array[x-1][y+1] && array[x-1][y+1] == array[x-2][y+2] && array[x-2][y+2] == array[x-3][y+3]{
                            checkWinner=array[x][y]
                            array[x][y] = 3
                            array[x-1][y+1] = 3
                            array[x-2][y+2] = 3
                            array[x-3][y+3] = 3
                            congratulatePlayer(winner: checkWinner)
                        }
                        
                        
                        
                        
                        
                    }
                    if y+3<5 && x+3 < 5{
                        if array[x][y] == array[x+1][y+1] && array[x+1][y+1] == array[x+2][y+2] && array[x+2][y+2] == array[x+3][y+3]{
                            checkWinner=array[x][y]
                            array[x][y] = 3
                            array[x+1][y+1] = 3
                            array[x+2][y+2] = 3
                            array[x+3][y+3] = 3
                            congratulatePlayer(winner: checkWinner)
                        }
                        
                    }
                    if y-3 > -1 && x+3<5{
                        if array[x][y] == array[x+1][y-1] && array[x+1][y-1] == array[x+2][y-2] && array[x+2][y-2] == array[x+3][y-3]{
                            checkWinner=array[x][y]
                            array[x][y] = 3
                            array[x+1][y-1] = 3
                            array[x+2][y-2] = 3
                            array[x+3][y-3] = 3
                            congratulatePlayer(winner: checkWinner)
                        }
                        
                    }
                    if y-3 > -1 && x-3 > -1{
                        if array[x][y] == array[x-1][y-1] && array[x-1][y-1] == array[x-2][y-2] && array[x-2][y-2] == array[x-3][y-3]{
                            checkWinner=array[x][y]
                            array[x][y] = 3
                            array[x-1][y-1] = 3
                            array[x-2][y-2] = 3
                            array[x-3][y-3] = 3
                            congratulatePlayer(winner: checkWinner)
                        }
                        
                    }
                    
                }
                
            }
        }
        
    }
    func addValue(row:Int){
        var x=4
        while x > -1{
            if array[x][row]==0{
                array[x][row]=turn
                updateViews()
                checkWinner()
                if winner == 0{
                    changeTurn()
                }
                x = -2
                
            }
            else{
                x=x-1
            }
            
        }
        checkTiedGame()
        
        
        
    }
    func congratulatePlayer(winner:Int){
        updateBackgrounds()
        showAlert(winner: winner)
        
    }
    func updateButtons(){
        if level==1{
            self.button1.setBackgroundImage(UIImage(named: "1green.png"), for: UIControlState.normal)
            self.button2.setBackgroundImage(UIImage(named: "2green.png"), for: UIControlState.normal)
            self.button3.setBackgroundImage(UIImage(named: "3green.png"), for: UIControlState.normal)
            self.button4.setBackgroundImage(UIImage(named: "4green.png"), for: UIControlState.normal)
            self.button5.setBackgroundImage(UIImage(named: "5green.png"), for: UIControlState.normal)
        }
        if level==2{
            
            self.button1.setBackgroundImage(UIImage(named: "1purple.png"), for: UIControlState.normal)
            self.button2.setBackgroundImage(UIImage(named: "2purple.png"), for: UIControlState.normal)
            self.button3.setBackgroundImage(UIImage(named: "3purple.png"), for: UIControlState.normal)
            self.button4.setBackgroundImage(UIImage(named: "4purple.png"), for: UIControlState.normal)
            self.button5.setBackgroundImage(UIImage(named: "5purple.png"), for: UIControlState.normal)
            
        }
        if level==3{
            self.button1.setBackgroundImage(UIImage(named: "1blue.png"), for: UIControlState.normal)
            self.button2.setBackgroundImage(UIImage(named: "2blue.png"), for: UIControlState.normal)
            self.button3.setBackgroundImage(UIImage(named: "3blue.png"), for: UIControlState.normal)
            self.button4.setBackgroundImage(UIImage(named: "4blue.png"), for: UIControlState.normal)
            self.button5.setBackgroundImage(UIImage(named: "5blue.png"), for: UIControlState.normal)
            
            
        }
    }
    
    func showAlert(winner:Int){
        if level==1{
            self.winner=winner
            updateCounters()
            let  alert = UIAlertController(title: "Felicidades!", message: "El jugador \(winner) es el ganador de este nivel", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ir al Nivel 2", style: UIAlertActionStyle.default, handler: { action in self.changeLevel()}))
            alert.addAction(UIAlertAction(title: "Salir", style: UIAlertActionStyle.default, handler: { action in self.performSegue(withIdentifier: "goMainMenu", sender: self)}))
            self.present(alert, animated: true, completion: nil)

        }
        if level==2{
            self.winner=winner
            updateCounters()
            let  alert = UIAlertController(title: "Felicidades!", message: "El jugador \(winner) es el ganador de este nivel", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ir al Nivel 3", style: UIAlertActionStyle.default, handler: { action in self.changeLevel()}))
            alert.addAction(UIAlertAction(title: "Salir", style: UIAlertActionStyle.default, handler: { action in self.performSegue(withIdentifier: "goMainMenu", sender: self)}))
            self.present(alert, animated: true, completion: nil)
            
        }
        if level==3{
            var marker1=0
            var marker2=0
            marker1=Int(counter1.text!)!
            marker2=Int(counter2.text!)!
            
            if winner==1{
                marker1 += 1
            }
            if winner==2{
                marker2 += 1
                
            }
            updateCounters()
            
            if marker1>marker2{
                self.winner=1
            }
            else{
                self.winner=2
                
            }
            
            
            let  alert = UIAlertController(title: "Felicidades!", message: "El jugador \(self.winner) es el ganador de los tres niveles", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Reiniciar Juego", style: UIAlertActionStyle.default, handler: { action in self.restartGame()}))
            alert.addAction(UIAlertAction(title: "Salir", style: UIAlertActionStyle.default, handler: { action in self.performSegue(withIdentifier: "goMainMenu", sender: self)}))
            self.present(alert, animated: true, completion: nil)

        }
        
    }
    
    func restartGame(){
        cleanArray()
        defineFirstTurn()
        level=1
        winner=0
        updateButtons()
        updateViews()
        counter1.text="0"
        counter2.text="0"
        updateBackgrounds()
        
        if Reachability.isConnectedToNetwork() == true{
            if self.interstitial.isReady{
                
                presentAd()
            }
        }

        
    }
    func changeLevel(){
        cleanArray()
        level=level+1
        if winner == 1{
            turn=2
            
            changeTurnIndicator()
        }
        else{
            turn=1
            
            changeTurnIndicator()
        }
        winner=0
        updateButtons()
        updateViews()
        updateBackgrounds()
    }
    
    func updateCounters(){
        var marker = 0
        if winner == 1{
            marker=Int(counter1.text!)!
            self.counter1.text = "\(marker+1)"
        }
        if winner == 2{
            marker=Int(counter2.text!)!
            self.counter2.text = String(marker+1)
        }
    }
    
    
    
    
    func updateViews(){
        
        updateCell(cell: c00, play: array[0][0])
        updateCell(cell: c10, play: array[0][1])
        updateCell(cell: c20, play: array[0][2])
        updateCell(cell: c30, play: array[0][3])
        updateCell(cell: c40, play: array[0][4])
        
        updateCell(cell: c01, play: array[1][0])
        updateCell(cell: c11, play: array[1][1])
        updateCell(cell: c21, play: array[1][2])
        updateCell(cell: c31, play: array[1][3])
        updateCell(cell: c41, play: array[1][4])
        
        updateCell(cell: c02, play: array[2][0])
        updateCell(cell: c12, play: array[2][1])
        updateCell(cell: c22, play: array[2][2])
        updateCell(cell: c32, play: array[2][3])
        updateCell(cell: c42, play: array[2][4])
        
        updateCell(cell: c03, play: array[3][0])
        updateCell(cell: c13, play: array[3][1])
        updateCell(cell: c23, play: array[3][2])
        updateCell(cell: c33, play: array[3][3])
        updateCell(cell: c43, play: array[3][4])
        
        updateCell(cell: c04, play: array[4][0])
        updateCell(cell: c14, play: array[4][1])
        updateCell(cell: c24, play: array[4][2])
        updateCell(cell: c34, play: array[4][3])
        updateCell(cell: c44, play: array[4][4])
    }
    func updateCell(cell:UIImageView, play:Int){
        if play == 0{
            cell.image=UIImage(named: "blankSpace.png")
        }
        if play == 1{
            cell.image=UIImage(named: "player1.png")
        }
        if play == 2{
            cell.image=UIImage(named: "player2.png")

        }
    }
    func updateBackgrounds(){
        
        updateBackGroundCell(cell: c00, play: array[0][0])
        updateBackGroundCell(cell: c10, play: array[0][1])
        updateBackGroundCell(cell: c20, play: array[0][2])
        updateBackGroundCell(cell: c30, play: array[0][3])
        updateBackGroundCell(cell: c40, play: array[0][4])
        
        updateBackGroundCell(cell: c01, play: array[1][0])
        updateBackGroundCell(cell: c11, play: array[1][1])
        updateBackGroundCell(cell: c21, play: array[1][2])
        updateBackGroundCell(cell: c31, play: array[1][3])
        updateBackGroundCell(cell: c41, play: array[1][4])
        
        updateBackGroundCell(cell: c02, play: array[2][0])
        updateBackGroundCell(cell: c12, play: array[2][1])
        updateBackGroundCell(cell: c22, play: array[2][2])
        updateBackGroundCell(cell: c32, play: array[2][3])
        updateBackGroundCell(cell: c42, play: array[2][4])
        
        updateBackGroundCell(cell: c03, play: array[3][0])
        updateBackGroundCell(cell: c13, play: array[3][1])
        updateBackGroundCell(cell: c23, play: array[3][2])
        updateBackGroundCell(cell: c33, play: array[3][3])
        updateBackGroundCell(cell: c43, play: array[3][4])
        
        updateBackGroundCell(cell: c04, play: array[4][0])
        updateBackGroundCell(cell: c14, play: array[4][1])
        updateBackGroundCell(cell: c24, play: array[4][2])
        updateBackGroundCell(cell: c34, play: array[4][3])
        updateBackGroundCell(cell: c44, play: array[4][4])

    }
    func updateBackGroundCell(cell:UIImageView, play:Int){
        if play == 3{
            
            cell.backgroundColor=UIColor(
                red: 0x5e/255,
                green: 0xaf/255,
                blue: 0xff/255,
                alpha: 1.0)
        }
        else{
            cell.backgroundColor=UIColor.white
        }
    }
    func cleanArray(){
        for x in 0 ..< array.count {
            for y in 0 ..< array[x].count {
                array[x][y]=0
            }
        }
    }
    func arrayIsFull()->Bool{
        var isFull = false
        for x in 0 ..< array.count {
            for y in 0 ..< array[x].count {
                if array[x][y]==0{
                   return false
                }
                
            }
        }
        return true
    }
    func checkTiedGame(){
        if arrayIsFull() && winner==0{
            indicateTieAlert()
        }
    }
    func indicateTieAlert(){
        if level==1{
            updateCounters()
            let  alert = UIAlertController(title: "Alerta!", message: "Este nivel terminó en empate", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ir al Nivel 2", style: UIAlertActionStyle.default, handler: { action in self.changeLevel()}))
            alert.addAction(UIAlertAction(title: "Salir", style: UIAlertActionStyle.default, handler: { action in self.performSegue(withIdentifier: "goMainMenu", sender: self)}))
            self.present(alert, animated: true, completion: nil)
            
        }
        if level==2{
            updateCounters()
            let  alert = UIAlertController(title: "Alerta!", message: "Este nivel terminó en empate", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ir al Nivel 3", style: UIAlertActionStyle.default, handler: { action in self.changeLevel()}))
            alert.addAction(UIAlertAction(title: "Salir", style: UIAlertActionStyle.default, handler: { action in self.performSegue(withIdentifier: "goMainMenu", sender: self)}))
            self.present(alert, animated: true, completion: nil)
            
        }
        if level==3{
            var marker1=0
            var marker2=0
            marker1=Int(counter1.text!)!
            marker2=Int(counter2.text!)!
            
            
            updateCounters()
            
            if marker1==marker2{
                let  alert = UIAlertController(title: "Alerta!", message: "El juego terminó en empate", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Reiniciar Juego", style: UIAlertActionStyle.default, handler: { action in self.restartGame()}))
                alert.addAction(UIAlertAction(title: "Salir", style: UIAlertActionStyle.default, handler: { action in self.performSegue(withIdentifier: "goMainMenu", sender: self)}))
                self.present(alert, animated: true, completion: nil)
            }
            
            
            
            
            
        }

        
    }

}
