//
//  Player.swift
//  2n2
//
//  Created by Cristopher Nunez Del Prado on 31/01/17.
//  Copyright © 2017 Cristopher Nunez Del Prado. All rights reserved.
//

import Foundation
class Player{
    var id:Int
    var cards=[Card]()
    var checks:Int
    init(r_id:Int, r_checks:Int){
        id=r_id
        checks=r_checks
        
    }
    
    func hasCards()->Bool{
        if cards.count > 0{
            return true
        }
        else{
            return false
        
        }
    }
    func receivePunishment(bunch: [Card]){
        cards.append(contentsOf: bunch)
    }
    func getFirstCard()->Card{
        return cards[0]
    }
    func deleteFirstCard(){
        if cards.count > 0 {
           cards.remove(at: 0)
        }
        
    }
    
}
